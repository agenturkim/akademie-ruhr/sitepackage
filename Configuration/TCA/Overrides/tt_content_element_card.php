<?php

defined('TYPO3_MODE') || die();

/***************
 * Add Content Element
 */
if (!is_array($GLOBALS['TCA']['tt_content']['types']['card'])) {
    $GLOBALS['TCA']['tt_content']['types']['card'] = [];
}

/***************
 * Add content element to selector list
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Karte',
        'card',
    ]
);

/***************
 * Configure element type
 */
$GLOBALS['TCA']['tt_content']['types']['card'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['card'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.header;header_minimal,
                header,
                header_link,
                image,
                bodytext,
                mode,
        ',
        'columnsOverrides' => [
            'image' => [
                'config' => [
                    'minitems' => 1,
                    'maxitems' => 1,
                ]
            ],
            'bodytext' => [
                'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel',
                'config' => [
                    'enableRichtext' => true,
                    'richtextConfiguration' => 'default'
                ]
            ]
        ]
    ]
);

/***************
 * Register fields
 */
$GLOBALS['TCA']['tt_content']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns'],
    [
        'mode' => [
            'label' => 'Darstellung',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Bild Links, Text Rechts', 0],
                    ['Bild Rechts, Text Links', 1],
                ],
            ],
        ],
    ]
);