<?php
defined('TYPO3_MODE') || die();

call_user_func(function () {
    // change tt_content subheader field to be a text type
    // and remove length restriction for editors
    // there might be cleaner way by now
    $GLOBALS['TCA']['tt_content']['columns']['subheader']['config']['type'] = 'text';
    unset($GLOBALS['TCA']['tt_content']['columns']['subheader']['config']['size'], $GLOBALS['TCA']['tt_content']['columns']['subheader']['config']['max']);
    $GLOBALS['TCA']['tt_content']['columns']['subheader']['config']['cols'] = '80';
    $GLOBALS['TCA']['tt_content']['columns']['subheader']['config']['rows'] = '8';
    $GLOBALS['TCA']['tt_content']['columns']['subheader']['config']['softref'] = 'typolink_tag,images,email[subst],urltext';
});

