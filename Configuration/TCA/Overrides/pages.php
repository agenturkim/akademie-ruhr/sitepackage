<?php
defined('TYPO3_MODE') || die();

call_user_func(function()
{
    /**
     * Temporary variables
     */
    $extensionKey = 'akademie_ruhr';

    /**
     * Default PageTS for AkademieRuhr
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
        $extensionKey,
        'Configuration/PageTS/All.txt',
        'Akademie Ruhr'
    );

    $GLOBALS['TCA']['pages']['columns']['title']['config']['max'] = 100;
});
