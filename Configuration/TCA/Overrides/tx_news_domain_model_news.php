<?php defined('TYPO3_MODE') || die();

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * Eigene Felder dem News-Plugin hinzufügen
 */
$prefix = 'tx_akademie_ruhr_';

// Bildnachweis
$fieldAttribution = $prefix . 'attribution';
// Überschrift Linke Box
$fieldLeftBoxHeading = $prefix . 'left_box_heading';
// Inhalt Linke Box
$fieldLeftBoxBody = $prefix . 'left_box_body';
// Link Linke Box
$fieldLeftBoxLink = $prefix . 'left_box_link';
// Überschrift Rechte Box
$fieldRightBoxHeading = $prefix . 'right_box_heading';
// Inhalt Rechte Box
$fieldRightBoxBody = $prefix . 'right_box_body';
// Link Rechte Box
$fieldRightBoxLink = $prefix . 'right_box_link';

$fields = [
    // Feld attribution => Bildnachweis
    $fieldAttribution => [
        'exclude' => 1,
        'label'   => 'Bildnachweis',
        'config'  => [
            'type' => 'text',
            'eval' => 'trim',
        ]
    ],
    $fieldLeftBoxHeading => [
        'exclude' => 1,
        'label'   => 'Überschrift Linke Box',
        'config'  => [
            'type' => 'text',
            'eval' => 'trim',
        ]
    ],
    $fieldLeftBoxBody => [
        'exclude' => 1,
        'label'   => 'Button Linke Box',
        'config'  => [
            'type' => 'input',
        ]
    ],
    $fieldLeftBoxLink => [
        'exclude' => 1,
        'label'   => 'Link Linke Box',
        'config'  => [
            'type'  => 'input',
            'renderType' => 'inputLink',
        ]
    ],
    $fieldRightBoxHeading => [
        'exclude' => 1,
        'label'   => 'Überschrift Rechte Box',
        'config'  => [
            'type' => 'text',
            'eval' => 'trim',
        ]
    ],
    $fieldRightBoxBody => [
        'exclude' => 1,
        'label'   => 'Button Rechte Box',
        'config'  => [
            'type' => 'input',
        ]
    ],
    $fieldRightBoxLink => [
        'exclude' => 1,
        'label'   => 'Link Rechte Box',
        'config'  => [
            'type'  => 'input',
            'renderType' => 'inputLink',
        ]
    ],

];

ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $fields);

// Assemble TCA String
// Generate a string of "x, y, z"
$customFields = [
    $fieldAttribution, 
    $fieldLeftBoxHeading, $fieldLeftBoxBody, $fieldLeftBoxLink,
    $fieldRightBoxHeading, $fieldRightBoxBody, $fieldRightBoxLink,
];
$TCAString = implode(", ", $customFields);

ExtensionManagementUtility::addFieldsToPalette(
    'tx_news_domain_model_news',
    $prefix . 'fields',
    $TCAString
);

ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', $TCAString);