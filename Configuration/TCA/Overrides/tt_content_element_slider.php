<?php

defined('TYPO3_MODE') || die();

/***************
 * Add Content Element
 */
if (!is_array($GLOBALS['TCA']['tt_content']['types']['slider'])) {
    $GLOBALS['TCA']['tt_content']['types']['slider'] = [];
}

/***************
 * Add content element to selector list
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Slider',
        'slider',
    ]
);

/***************
 * Configure element type
 */
$GLOBALS['TCA']['tt_content']['types']['slider'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['slider'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.header;header_minimal,
                image,
        ',
        'columnsOverrides' => [
            'image' => [
                'config' => [
                    'minitems' => 1,
                ]
            ],
        ]
    ]
);

/***************
 * Register fields
 */
$GLOBALS['TCA']['sys_file_reference']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['sys_file_reference']['columns'],
    [
        'colormode' => [
            'label' => 'Darstellung',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Helle Schriftfarbe (bspw. bei Fotos)', 'image-is-dark'],
                    ['Dunkle Schriftfarbe (bspw. bei Zeichnungen)', 'image-is-light'],
                ],
            ],
        ],
    ]
);

$GLOBALS['TCA']['sys_file_reference']['palettes']['imageoverlayPalette']['showitem'] .= ', colormode';
