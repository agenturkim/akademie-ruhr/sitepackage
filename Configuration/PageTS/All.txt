#
# PageTS for Akademie Ruhr
#
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:akademie_ruhr/Configuration/PageTS/RTE.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:akademie_ruhr/Configuration/PageTS/TCEFORM.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:akademie_ruhr/Configuration/PageTS/TCEMAIN.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:akademie_ruhr/Configuration/PageTS/Mod/WebLayout/BackendLayouts.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:akademie_ruhr/Configuration/PageTS/ContentElement/Wizard.tsconfig">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:akademie_ruhr/Configuration/PageTS/ContentElement/Element/Card.tsconfig">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:akademie_ruhr/Configuration/PageTS/ContentElement/Element/Slider.tsconfig">