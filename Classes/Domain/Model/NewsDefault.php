<?php

namespace RuhrConnectGmbh\AkademieRuhr\Domain\Model;

class NewsDefault extends \GeorgRinger\News\Domain\Model\NewsDefault
{
    /** @var string */
    protected $txAkademieRuhrAttribution;

    /** @var string */
    protected $txAkademieRuhrLeftBoxHeading;

    /** @var string */
    protected $txAkademieRuhrLeftBoxBody;

    /** @var string */
    protected $txAkademieRuhrLeftBoxLink;

    /** @var string */
    protected $txAkademieRuhrRightBoxHeading;

    /** @var string */
    protected $txAkademieRuhrRightBoxBody;

    /** @var string */
    protected $txAkademieRuhrRightBoxLink;

    /**
     * @param string $txAkademieRuhrAttribution
     */
    public function setTxAkademieRuhrAttribution(string $txAkademieRuhrAttribution): void
    {
        $this->txAkademieRuhrAttribution = $txAkademieRuhrAttribution;
    }

    /**
     * @return string
     */
    public function getTxAkademieRuhrAttribution(): string
    {
        return $this->txAkademieRuhrAttribution;
    }

    public function getTxAkademieRuhrLeftBoxHeading(): string
    {
        return $this->txAkademieRuhrLeftBoxHeading;
    }

    public function setTxAkademieRuhrLeftBoxHeading(string $txAkademieRuhrLeftBoxHeading): void
    {
        $this->txAkademieRuhrLeftBoxHeading = $txAkademieRuhrLeftBoxHeading;
    }

    public function getTxAkademieRuhrLeftBoxBody(): string
    {
        return $this->txAkademieRuhrLeftBoxBody;
    }

    public function setTxAkademieRuhrLeftBoxBody(string $txAkademieRuhrLeftBoxBody): void
    {
        $this->txAkademieRuhrLeftBoxBody = $txAkademieRuhrLeftBoxBody;
    }
    
    public function getTxAkademieRuhrLeftBoxLink(): string
    {
        return $this->txAkademieRuhrLeftBoxLink;
    }

    public function setTxAkademieRuhrLeftBoxLink(string $txAkademieRuhrLeftBoxLink): void
    {
        $this->txAkademieRuhrLeftBoxLink = $txAkademieRuhrLeftBoxLink;
    }
    public function getTxAkademieRuhrRightBoxHeading(): string
    {
        return $this->txAkademieRuhrRightBoxHeading;
    }

    public function setTxAkademieRuhrRightBoxHeading(string $txAkademieRuhrRightBoxHeading): void
    {
        $this->txAkademieRuhrRightBoxHeading = $txAkademieRuhrRightBoxHeading;
    }

    public function getTxAkademieRuhrRightBoxBody(): string
    {
        return $this->txAkademieRuhrRightBoxBody;
    }

    public function setTxAkademieRuhrRightBoxBody(string $txAkademieRuhrRightBoxBody): void
    {
        $this->txAkademieRuhrRightBoxBody = $txAkademieRuhrRightBoxBody;
    }
    
    public function getTxAkademieRuhrRightBoxLink(): string
    {
        return $this->txAkademieRuhrRightBoxLink;
    }

    public function setTxAkademieRuhrRightBoxLink(string $txAkademieRuhrRightBoxLink): void
    {
        $this->txAkademieRuhrRightBoxLink = $txAkademieRuhrRightBoxLink;
    }

    public function getHasTxAkademieRuhrLeftBox(): bool
    {
        return $this->txAkademieRuhrLeftBoxHeading && $this->txAkademieRuhrLeftBoxBody && $this->txAkademieRuhrLeftBoxLink;
    }

    public function getHasTxAkademieRuhrRightBox(): bool
    {
        return $this->txAkademieRuhrRightBoxHeading && $this->txAkademieRuhrRightBoxBody && $this->txAkademieRuhrRightBoxLink;
    }

    public function getHasTxAkademieRuhrBoxes(): bool
    {
        return $this->getHasTxAkademieRuhrLeftBox() || $this->getHasTxAkademieRuhrRightBox();
    }

    public function getShowTxAkademieRuhrMediaContainer(): bool
    {
        return !in_array($this->getFirstCategory()->getTitle(), ['Interviews', 'Erfolgsstories'], true);
    }

}
