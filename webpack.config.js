const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

const sourceDirectory = 'Build';
const targetDirectory = 'Resources/Public/Dist'
const publicPath = `/typo3conf/ext/akademie_ruhr/${targetDirectory}/`

module.exports = {
    module: {
        rules: [
            {
                test: /\.(s[ac]ss|css)$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)?(\?[a-z0-9#=&.]+)?$/,
                type: 'asset/resource',
            
            },
        ]
    },
    entry: {
        app: {
            import: `./${sourceDirectory}/Js/app.js`,
            dependOn: 'lazy',
        },
        lazy: {
            import: `./${sourceDirectory}/Js/lazy.js`,
        },
        swiper: {
            import: `./${sourceDirectory}/Js/components/swiper.js`,
            dependOn: 'app'
        },
        gallery: {
            import: `./${sourceDirectory}/Js/galleries.js`,
            dependOn: 'app'

        }
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, targetDirectory),
        publicPath,
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
    ],
    optimization: {
        runtimeChunk: 'single',
    }
}
