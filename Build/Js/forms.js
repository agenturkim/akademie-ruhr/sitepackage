import $ from 'jquery';

$(document).ready(function () {

    const PRIVACY_HTML =  `Durch das Absenden erkläre ich mich mit der Verarbeitung meiner persönlichen Daten gemäss der <a href="index.php?id=81">Datenschutzerklärung</a> einverstanden *`;

    const isForm = document.forms.length === 1;
    let $form;

    if (isForm) {
       $form = $(document.forms[0]);
        let $checkElement = $(
            `<div class="form-group">
            <div class="custom-control custom-checkbox">
            <input required type="checkbox" class="custom-control-input" id="privacy">
            <label style="font-size: .825em;" class="custom-control-label d-block" for="privacy">${PRIVACY_HTML}</label>
          </div></div>`
        );

        $checkElement.insertAfter($form.find(".form-group").last());

    }

});
