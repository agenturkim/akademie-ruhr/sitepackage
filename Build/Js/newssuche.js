import $ from 'jquery';

$(document).ready(function () {

    let $selectCategory = $('#news-filter-select');
    let $submitButton = $('#news-filter-button');

    $selectCategory.on('change', function () {
        $submitButton.attr('href', $(this).val());
    });

});
