import $ from 'jquery';

$(document).ready(function() {
    /*
     * Mitscrollende Navi
     */
    let lastScrollTop = 0;
    const navbar = $('#scrollingNav');
    const navbarHeight = navbar.outerHeight();
    const $header = $('header').first();
    const initialNavbarHeight = navbar.height();

    if (isMobile()) {
        // $header.css('height', initialNavbarHeight);
    } else {
        // $header.css('height', initialNavbarHeight + $('.breadcrumb-section').height());
    }

    if (! isMobile()) {
    $(window).scroll(function () {
        hasScrolled();
    });
}

    /**
     * Hier kann festgelegt werden, wann die Navi mitscrollen soll.
     */
    function enableScrollingNav() {
        let isDesktop = window.innerWidth > 768;


        return isDesktop;
    }

    function isMobile()
    {
        let isMobile = window.innerWidth <= 768;


        return isMobile;
    }

    function hasScrolled() {

        if (enableScrollingNav()) {

            let st = $(window).scrollTop();
            let shouldHaveClass = st > 0;
            let shouldBeUp = st > lastScrollTop && st > navbarHeight && shouldHaveClass;
            let shouldBeDown = st + $(window).height() < $(document).height() && shouldHaveClass && !shouldBeUp;

            if (shouldBeUp) {
                navbar.css('top', -(navbar.height() * 2));
            } else if (shouldBeDown) {
                navbar.css('top', '');
            }

           // navbar.toggleClass('nav-up', shouldBeUp);
           // navbar.toggleClass('nav-down', shouldBeDown);

            lastScrollTop = st;

        }
    }
    /*
     * Dropdown Menü
     */
    let $dropdownItemsWithMedia = $('.dropdown-child-item-has-media');

    $dropdownItemsWithMedia.on('mouseover', function () {
        let $thisID = $(this).attr('data-show-media');
        let $thisMediaContainerID = $(this).attr('data-media-container');
        let $thisMedia = $(`#dynamicMedia-item-${$thisID}`);
        let $dynamicMediaContainer = $(`#dynamicMedia-${$thisMediaContainerID}`);

        function getCurrentMedia() {
            let $currentMedia;

            $dynamicMediaContainer.children().each(function () {
                if (! $(this).hasClass('d-none')) {
                    $currentMedia = $(this);
                }
            })

            return $currentMedia;
        }

        let $currentMedia = getCurrentMedia();
        if ($currentMedia) {
            if ($currentMedia.attr('id') !== $thisID) {
                $currentMedia.addClass('d-none');

                if ($thisMedia.hasClass('d-none')) {
                    $thisMedia.removeClass('d-none').fadeIn();
                }
            }
        }

    })
})
