import $ from 'jquery';
import './components/sliphover';

import './../Css/content/_gallery.scss'

/**
 * Galerie Übersichtsseiten Filterfunktion
 *
 */
$(document).ready(function () {
    const slipHoverTargetSelector = "img.slip[title!=''][title]";
    const $slipHoverImages = $(slipHoverTargetSelector);
    let $filter = $('#gallery-gallery-filter');

    if ($filter && $filter.length) {

        let $gridSelector = $('#gallery-gallery-grid');

        // Bind Filter
        $filter.on('change', function () {
            let val = $(this).val();

            if (val === "*") {
                $gridSelector.children().show();
            } else {
                $(val).show();
                $(val).siblings().hide();
            }
        });

    }

    // SlipHover
    if ($slipHoverImages && $slipHoverImages.length && window.innerWidth >= 768) {

        $slipHoverImages
        .parents('.ce-gallery')
        .sliphover({
            target: slipHoverTargetSelector,
            withLink: false,
        });

        $("body").on("click", function (e) {
            if (e.target.nodeName  === "DIV" && e.target.parentNode.className === "sliphover-overlay" && e.target.parentNode.sliphoverRef) {
                $(e.target.parentNode.sliphoverRef).parent().click();
            }
        })
    }

});
