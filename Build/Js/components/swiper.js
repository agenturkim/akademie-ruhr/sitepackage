import $ from 'jquery';
import { Swiper, Pagination, Lazy, Autoplay } from 'swiper';
import "swiper/swiper-bundle.css";
import "../../Css/content/_slider.scss";

Swiper.use([Autoplay, Pagination, Lazy]);

$(document).ready(function () {
    new Swiper('.swiper-container', {
        // Optional parameters
        preloadImages: false,
        lazy: true,
        loop: true,
        autoplay: {
            delay: 7000,
        },
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }
    });
})
