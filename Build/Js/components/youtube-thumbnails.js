import $ from 'jquery';

$(function() {
    $(".vurpl-youtube").each(function() {
        // set the videothumbnail as background
        // maxresdefault/high if width > 640 else standard/high
        if ($(this).width() > 640) var img_src = $(this).data("thumb-large");
        else	var img_src = $(this).data("thumb-medium");
        $(this).css('background-image', 'url(' + img_src + ')');

        // Overlay the Play icon to make it look like a video player
        $(this).append($('<div/>', {'class': 'play'}));

        // use nocookie domain
        if ($(this).data('nocookie')) var youtubeDomain = "www.youtube-nocookie.com";
        else var youtubeDomain = "www.youtube.com";

        $(document).delegate('#'+this.id, 'click', function() {
            // Create an iframe with autoplay set to true
            var iframe_url = "https://" + youtubeDomain + "/embed/" + this.id + "?autoplay=1&autohide=1";
            if ($(this).data('params')) iframe_url+='&'+$(this).data('params');
            var iframe = $('<iframe/>', {'src': iframe_url });
            // Replace the YouTube thumbnail with YouTube HTML5 Player
            $(this).html(iframe);

        });
    });
});
