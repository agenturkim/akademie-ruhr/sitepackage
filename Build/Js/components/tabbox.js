import $ from 'jquery';

$(() => {
    /* Mobile Navigation der Reiterboxen */
    $("#mobile-tabbed-nav select")
        .on("change", function () {
            $(`[data-uid='${$(this).val()}']`)[0].click();
        });
});
