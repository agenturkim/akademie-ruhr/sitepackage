/*
 * Animationen und sonstiges Eye-Candy
 *
 * in Verbindung mit _effects.scss
 */
import $ from 'jquery';

$(() => {
    /* An den Anfang der Seite scrollen */
    $('.jumptop')
        .on("click", () => $("html, body").animate({ scrollTop: 0 }, "slow"));
    /* Bild-Zoom Effekt bei mouseover / mouseleave */
    $('.zoom-in-out')
        .parent()
        .css('overflow', 'hidden');
    /* Mobile Navi */
    $('#mobileNav .navbar-nav').on('show.smapi', function(e, menu) {
        $(menu)
            .prev()
            .find('.sub-arrow')
            .css('transform', 'rotate(90deg)');
    });
    $('#mobileNav .navbar-nav').on('hide.smapi', function(e, menu) {
        $(menu)
            .prev()
            .find('.sub-arrow')
            .css('transform', '')
    });
    /* $('.navbar-toggler')
        .css(
            "background-image",
             "url(\"data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(0, 0, 0, 0.5)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 7h22M4 15h22M4 23h22'/%3E%3C/svg%3E\")"
        )
        .css(
            "background-image",
            "url(\"data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(0, 0, 0, 0.5)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M 10,10 L 20,20 M 20,10 L 10,20'/%3E%3C/svg%3E\")"
        ) */

        let $navbarToggler = $(".navbar-toggler-icon");
        $('#mobile').on('show.bs.collapse', function () {
            $navbarToggler.css(
                "background-image",
                "url(\"data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(0, 0, 0, 0.5)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M 10,10 L 20,20 M 20,10 L 10,20'/%3E%3C/svg%3E\")"
            )
        })

        $('#mobile').on('hide.bs.collapse', function () {
            $navbarToggler.attr("style", "");
        })

});
