import $ from 'jquery';

$(document).ready(function () {

    let $filters = $('.filter-studienort');

    let $filterField = $('#filter-fields');
    let $filterCity = $('#filter-cities');

    let $resultContainer = $('#filter-results');
    let $listContainer = $('#c5159');

    $filterCity.on('change', function () {

        $('#filter-fields option').show();

        let city = $(this).val();
        let $availableFields = $(`[data-city='${city}']`);
        let theseFields = [];
        $availableFields.each(function () {
            theseFields.push($(this).data('field'));
        });

        let oSelector = `#filter-fields option[value!=''][value!=-1]`;
        $(theseFields).each(function (i, val) {
            oSelector += `[value!=${val}]`;
        });

        $(oSelector).hide();

        if ($(this).val() === "") {
            $filters.find("option").show();
        }

        $listContainer.show();
        $resultContainer.hide().empty();

        let selector = '';


        if ($filterField.val()) {
            selector += `[data-field='${$filterField.val()}']`;
        }

        if ($filterCity.val()) {
            selector += `[data-city='${$filterCity.val()}']`;
        }


        let $targetElements = $(selector).parent().clone();

        if ($targetElements && $targetElements.length > 0) {

            let $html = $('<div></div>');

            let items = 0;
            let rows = [];
            let columns = [];
            $targetElements.each(function () {
                items++;
                columns.push(this);
                if (items % 4 === 0) {
                    rows.push(columns);
                    columns = [];
                }
            })

            if (rows.length === 0) {
                rows.push(columns);
            }

            $(rows).each(function () {

                let $rowHtml = $('<div class="row my-0 my-md-3"></div>');
                $(this).each(function () {
                    $rowHtml.append(this);
                })
                $html.append($rowHtml);
            });

            $resultContainer.html($html);
            $resultContainer.show();
            $listContainer.hide();

        } else {
            $resultContainer.hide();
            $listContainer.show();
        }

        window.loazyLoadInstance.update();
    });

    $filterField.on('change', function () {

        $('#filter-cities option').show();

        let field = $(this).val();
        let $availableCities = $(`[data-field='${field}']`);
        let theseCities = [];
        $availableCities.each(function () {
            theseCities.push($(this).data('city'));
        });

        let oSelector = `#filter-cities option[value!=''][value!=-1]`;
        $(theseCities).each(function (i, val) {
            oSelector += `[value!=${val}]`;
        });

        $(oSelector).hide();

        if ($(this).val() === "") {
            $filters.find("option").show();
        }

        $listContainer.show();
        $resultContainer.hide().empty();

        let selector = '';


        if ($filterField.val()) {
            selector += `[data-field='${$filterField.val()}']`;
        }

        if ($filterCity.val()) {
            selector += `[data-city='${$filterCity.val()}']`;
        }


        let $targetElements = $(selector).parent().clone();

        if ($targetElements && $targetElements.length > 0) {

            let $html = $('<div></div>');

            let items = 0;
            let rows = [];
            let columns = [];
            $targetElements.each(function () {
                items++;
                columns.push(this);
                if (items % 4 === 0) {
                    rows.push(columns);
                    columns = [];
                }
            })

            if (rows.length === 0) {
                rows.push(columns);
            }

            $(rows).each(function () {

                let $rowHtml = $('<div class="row my-0 my-md-3"></div>');
                $(this).each(function () {
                    $rowHtml.append(this);
                })
                $html.append($rowHtml);
            });

            $resultContainer.html($html);
            $resultContainer.show();
            $listContainer.hide();

        } else {
            $resultContainer.hide();
            $listContainer.show();
        }

        window.loazyLoadInstance.update();
    });



});
