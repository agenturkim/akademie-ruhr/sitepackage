import LazyLoad from "vanilla-lazyload";

document.addEventListener("DOMContentLoaded", (() => {
    window.loazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy",
    });
}));
