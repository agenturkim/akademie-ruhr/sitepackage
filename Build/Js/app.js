'use strict';

// Dependencies
import './../Css/app.scss';

import 'popper.js';
import 'bootstrap';
import 'smartmenus/dist/jquery.smartmenus';
import 'smartmenus-bootstrap-4/jquery.smartmenus.bootstrap-4';

import './navigation';
import './galleries';
import './unisuche';
import './newssuche';
import './forms';
import './effects';

// Components
import './components/youtube-thumbnails';
import './components/sd-lightbox';
import './components/tabbox';
