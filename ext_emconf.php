<?php

$EM_CONF[$_EXTKEY] = [
    'title' => '[Akademie Ruhr] Template',
    'description' => 'Template-Erweiterung',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-11.5.99',
            'fluid_styled_content' => '10.4.0-11.5.99',
            'rte_ckeditor' => '10.4.0-11.5.99',
            'form' => '10.4.0-11.5.99',
            'news' => '*',
        ],
    ],
    'state' => 'stable',
    'author' => 'Pascale Beier',
    'author_email' => 'mail@pascalebeier.de',
    'author_company' => 'https://pascalebeier.de',
    'version' => '2.1.17',
    'autoload' => [
        'psr-4' => [
            'RuhrConnectGmbH\\AkademieRuhr\\' => 'Classes/'
        ]
    ],
];
