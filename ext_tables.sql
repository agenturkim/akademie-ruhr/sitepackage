#
# Extend Table structure for table 'tx_news_domain_model_news'
#
CREATE TABLE tx_news_domain_model_news (
    # Bildnachweis
	  tx_akademie_ruhr_attribution VARCHAR(255) DEFAULT '' NOT NULL,
    # Überschrift Linke Box
    tx_akademie_ruhr_left_box_heading VARCHAR(255) DEFAULT '' NOT NULL,
    # Inhalt Linke Box
    tx_akademie_ruhr_left_box_body VARCHAR(255) DEFAULT '' NOT NULL,
    # Link Linke Box
    tx_akademie_ruhr_left_box_link VARCHAR(255) DEFAULT '' NOT NULL,
    # Überschrift Rechte Box
    tx_akademie_ruhr_right_box_heading VARCHAR(255) DEFAULT '' NOT NULL,
    # Inhalt Rechte Box
    tx_akademie_ruhr_right_box_body VARCHAR(255) DEFAULT '' NOT NULL,
    # Link Rechte Box
    tx_akademie_ruhr_right_box_link VARCHAR(255) DEFAULT '' NOT NULL,
    # Clickstorm SEO
    tx_csseo int(11) unsigned NOT NULL default '0'
);

CREATE TABLE sys_file_reference (
    # Farbmodi für Slider
    colormode VARCHAR(255) DEFAULT '' NOT NULL
);
