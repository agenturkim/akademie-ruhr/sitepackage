Akademie Ruhr Template
=======================

### JavaScript und CSS

Assets liegen innerhalb Build/. Ein Buildscript ist in der package.json hinterlegt und wird über

`npm install` - Frontend-Pakete installieren
`npm run watch` - Dateien werden automatisch neu generiert wenn Quelldateien geändert werden
`npm run production` - Dateien für Livebetrieb minimieren

gesteuert.

Am einfachsten über die Kommandozeile in das Verzeichnis der akademie_ruhr Extension wechseln und

`npm run watch` ausführen.

Build/Css/ wird bspw. zu Resources/Public/Dist/app.css
