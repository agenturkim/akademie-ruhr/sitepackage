<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['akademie_ruhr'] = 'EXT:akademie_ruhr/Configuration/RTE/Default.yaml';

// Extend news
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/NewsDefault'][] = 'akademie_ruhr';
